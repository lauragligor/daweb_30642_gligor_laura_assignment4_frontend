import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

class AboutUs extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ro: localStorage.getItem('ro'),
            en: localStorage.getItem('en')
        };
        this.setStates = this.setStates.bind(this);
    }

    setStates(){
        this.setState({ro: localStorage.getItem('ro')})
        this.setState({en: localStorage.getItem('en')})
    }
    render() {
        const hs = {
            marginLeft: "30px",
            marginTop: "30px",
            textAlign: "justify"
        };
        const ps = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const imgs = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const columns = {
          float: 'left',
          width: '32%',
          padding: '5px'
        }

        const rows = {
          content: "",
          clear: 'both',
          display: 'table'
        }
        if(this.state.ro === 'true'){
        return(
        <div className = "AboutUsClass">
            <h1 style={hs}>Despre noi</h1>
            	<br /><br />
            	<h3 style={hs}>Cine suntem?</h3>
            	<p style={ps}>   Suntem un grup de oameni dornici sa raspunda nevoilor oamenilor din jurul nostru,
                cautand sa le oferim intotdeauna ceea ce ei cauta, la cele mai bune preturi.
               	</p>
            	<br />
            	<h3 style={hs}>Ce facem?</h3>
            	<p style={ps}>   Va aducem cele mai noi oferte din domeniul spatiilor imobiliare la case, apartamente si garsoniere.
                 Cerintele si nevoile clientilor nostri sunt pe primul loc, iar noi oferim o gama larga de servicii de cumparare sau
                 inchiriere a spatiilor dorite.
               	</p>
            	<br />
            	<h3 style={hs}>Ce oferim?</h3>
            	<p style={ps}>Fie ca sunteti in cautarea unui apartament pe care doriti sa-l inchiriati pentru o anumita perioada,
            	fie ca familia se mareste si va doriti un spatiu mai mare oferit de o casa, fie ca
            	doriti un spatiu de birouri de dimensiune mica, noi suntem solutia!</p>
            	<br /><br />
            	<p style={ps}>Va aducem mereu cele mai bune oferte, la cele mai avantajoase preturi de pe
            	piata!</p>
            	<br /><br />

            	<p style={ps}>Mai multe detalii despre noi gasiti in pagina de Contact. Pentru orice intrebari, nu exitati sa ne contactati!</p>
            	<br /><br />

            </div>
         )
         }
          else if(this.state.en === 'true') {
         return(
                 <div className = "AboutUsClass">
                     <h1 style={hs}>About Us</h1>
                     	<br /><br />
                     	<h3 style={hs}>Who are we?</h3>
                     	<p style={ps}>   We are a group of people eager to meet the needs of the people around us,
                                                                  always looking to offer them what they are looking for, at the best prices.
                        	</p>
                     	<br />
                     	<h3 style={hs}>What do we do?</h3>
                     	<p style={ps}>   We bring you the latest offers in the field of real estate for homes, apartments and studios.
                                                                   The requirements and needs of our customers come first, and we offer a wide range of purchasing services
                                                                   rental of the desired spaces.
                        	</p>
                     	<br />
                     	<h3 style={hs}>What do we offer?</h3>
                     	<p style={ps}>Whether you are looking for an apartment that you want to rent for a certain period,
                                                           either the family is growing and you want more space provided by a house, or
                                                           you want a small office space, we are the solution!</p>
                     	<br /><br />
                     	<p style={ps}>We always bring you the best offers, at the best prices on
                                                           market!</p>
                     	<br /><br />

                     	<p style={ps}>More details about us can be found on the Contact page. For any questions, do not hesitate to contact us!</p>
                     	<br /><br />

                     </div>
                  )
                  }
    };
}

export default AboutUs