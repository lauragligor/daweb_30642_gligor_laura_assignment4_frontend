import React from 'react';

class Home extends React.Component {
    setData(){
        localStorage.setItem('en', true);
        localStorage.setItem('ro', false);
        console.log("Ro: " + localStorage.getItem('ro'));
        console.log("En: " + localStorage.getItem('en'));
    }

    setData1(){
        localStorage.setItem('en', false);
        localStorage.setItem('ro', true);
        console.log("Ro: " + localStorage.getItem('ro'));
        console.log("En: " + localStorage.getItem('en'));
    }

    render(){
        return(
            <header>
                <div className="row">
                    <div className="columnn">
                        <div className="lang-menu">
                          <div class="selected-lang">Romana</div>
                            <ul className="langu">
                                <li onClick={() => this.setData()}><a class="en">English</a></li>
                                <li onClick={() => this.setData1()}><a class="ro">Romana</a></li>
                            </ul>
                        </div>
                     </div>
                     <div class="column">
                            <div class="menubar">

                                <ul class="btns">
                                    <li><a href="home">Acasa</a></li>
                                    <li><a href="news">Noutati</a></li>
                                    <li><a href="aboutus">Despre Noi</a></li>
                                    <li><a href="#">Inchirieri</a>
                                        <div class="submenu1">
                                        <ul>
                                            <li><a href="rentinggars">Garsoniere</a></li>
                                            <li><a href="rentingapart">Apartamente</a></li>
                                            <li><a href="rentinghouse">Case</a></li>
                                        </ul>
                                        </div>
                                    </li>
                                    <li><a href="#">Vanzari</a>
                                        <div class="submenu2">
                                        <ul>
                                            <li><a href="sellinggars">Garsoniere</a></li>
                                            <li><a href="sellingapart">Apartamente</a></li>
                                            <li><a href="sellinghouse">Case</a></li>
                                        </ul>
                                        </div>
                                    </li>
                                    <li><a href="contact">Contact</a></li>
                                    <li><a href="login">Login</a></li>
                                    <li><a href="register">Register</a></li>
                                    <li><a href="client">My Account</a></li>
                                </ul>
                            </div>
                        </div>
                </div>
            </header>
        )
    }
}

export default Home;