import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

class SellingApart extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ro: localStorage.getItem('ro'),
            en: localStorage.getItem('en')
        };
        this.setStates = this.setStates.bind(this);
    }

    setStates(){
        this.setState({ro: localStorage.getItem('ro')})
        this.setState({en: localStorage.getItem('en')})
    }
    render() {
        const hs = {
            marginLeft: "30px",
            marginTop: "30px",
            textAlign: "justify"
        };
        const ps = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const imgs = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const columns = {
          float: 'left',
          width: '32%',
          padding: '5px'
        }

        const rows = {
          content: "",
          clear: 'both',
          display: 'table'
        }
        if(this.state.ro === 'true'){
        return(
        <div className = "SellingApartClass">
            <h1 style={hs}>Vanzari apartamente</h1>
            	<br /><br /><br />
            	<p style={ps}>    Aici veti putea gasi ofertele noastre de apartamente pentru vanzare.
            	</p>
            	<br />
            	<p style={ps}>Nu toate  apartamentele propuse spre vanzare sunt complet mobilate si utilate.
            	La cele care nu sunt mobilate se specifica acest lucru in descriere.</p>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/ap10.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Someseni, 45mp, nemobilat</p>
            			<p style={ps}>Pret: 1600.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/ap9.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Zorilor, 50mp, nemobilat</p>
            			<p style={ps}>Pret: 210.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/ap8.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Manastur, 39mp</p>
            			<p style={ps}>Pret: 220.000 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/ap3.jpg")} width="400" height="300" />
            			<p style={ps}>Centru, 40mp</p>
            			<p style={ps}>Pret: 300.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/ap1.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Buna Ziua, 55mp</p>
            			<p style={ps}>Pret: 310.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/ap2.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Intre Lacuri, 49mp, nemobilat</p>
            			<p style={ps}>Pret: 220.000 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/ap5.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Someseni, 39mp</p>
            			<p style={ps}>Pret: 250.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/ap8.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Zorilor, 45mp</p>
            			<p style={ps}>Pret: 280.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/ap6.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Iris, 55mp, nemobilat</p>
            			<p style={ps}>Pret: 230.000 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            </div>
         )
         }
          else if(this.state.en === 'true') {
                return(
                        <div className = "SellingApartClass">
                            <h1 style={hs}>Selling apartaments</h1>
                            	<br /><br /><br />
                            	<p style={ps}>    Here you can find our offers of apartments for sale.
                            	</p>
                            	<br />
                            	<p style={ps}>Not all apartments offered for sale are fully furnished and equipped.
                                                            For those who are not furnished, this is specified in the description.</p>
                            	<br /><br />
                            	<div style={rows}>
                            	    <div style={columns}>
                            			<img style={imgs} src={require("./images/ap10.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Someseni, 45mp, nemobilat</p>
                            			<p style={ps}>Price: 1600.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/ap9.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Zorilor, 50mp, nemobilat</p>
                            			<p style={ps}>Price: 210.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/ap8.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Manastur, 39mp</p>
                            			<p style={ps}>Price: 220.000 euro</p>
                            			<br />
                            		</div>
                            	</div>
                            	<br /><br />
                            	<div style={rows}>
                            	    <div style={columns}>
                            			<img style={imgs} src={require("./images/ap3.jpg")} width="400" height="300" />
                            			<p style={ps}>Centru, 40mp</p>
                            			<p style={ps}>Price: 300.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/ap1.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Buna Ziua, 55mp</p>
                            			<p style={ps}>Price: 310.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/ap2.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Intre Lacuri, 49mp, nemobilat</p>
                            			<p style={ps}>Price: 220.000 euro</p>
                            			<br />
                            		</div>
                            	</div>
                            	<br /><br />
                            	<div style={rows}>
                            	    <div style={columns}>
                            			<img style={imgs} src={require("./images/ap5.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Someseni, 39mp</p>
                            			<p style={ps}>Price: 250.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/ap8.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Zorilor, 45mp</p>
                            			<p style={ps}>Price: 280.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/ap6.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Iris, 55mp, nemobilat</p>
                            			<p style={ps}>Price: 230.000 euro</p>
                            			<br />
                            		</div>
                            	</div>
                            	<br /><br />
                            </div>
                         )
          }
    };
}

export default SellingApart