import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import { Helmet } from 'react-helmet';

class News extends React.Component {

    constructor(props) {
        super(props)
        this.state = {
            ro: localStorage.getItem('ro'),
            en: localStorage.getItem('en')
        };
        this.setStates = this.setStates.bind(this);
    }

    setStates(){
        this.setState({ro: localStorage.getItem('ro')})
        this.setState({en: localStorage.getItem('en')})
    }

    render() {
        const hs = {
            marginLeft: "30px",
            marginTop: "30px",
            textAlign: "justify"
        };
        const ps = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const imgs = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const columns = {
          float: 'left',
          width: '32%',
          padding: '5px'
        }

        const rows = {
          content: "",
          clear: 'both',
          display: 'table'
        }
        const bck ={
            backgroundImage: "./images/gray.jpg"
        }
        console.log('ro:' + this.state.ro)
        console.log('en:'  +this.state.en)
        if(this.state.ro === 'true'){
            return(
            <div className = "NewsClass" style={bck}>

                <h1 style={hs}>Noutati</h1>
            	<br /><br /><br />
            	<p style={ps}>    In aceasta sectiune se gasesc cele mai noi oferte pe care firma noastra
            	le propune spre comercializare, indiferent daca doriti sa cumparati un spatiu sau
            	doar sa il inchiriati. </p>
            	<br />
            	<p style={ps}>Va aducem mereu cele mai bune oferte, la cele mai avantajoase preturi de pe
            	piata!</p>
            	<br />
            	<p style={ps}>Avem o gama larga de spatii situate in diverse zone ale orasului, oferindu-le
            	astfel clientilor nostri posibilitatea de a opta atat pentru zone periferice, linistite,
            	cat si pentru zone centrale, accesibile oricui.</p>
            	<br />
            	<p style={ps}>Cele mai noi oferte ale momentului sunt urmatoarele si se gasesc fiecare in pagina corespunzatoare categoriei
            	din care fac parte: </p>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require('./images/gars1.jpg')} width="400" height="350" />
            			<p style={ps}>Garsoniera de vanzare</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/ap1.jpg")} width="400" height="350" />
            			<p style={ps}>Apartament de inchiriat</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/casa1.jpg")} width="400" height="350" />
            			<p style={ps}>Casa de vanzare</p>
            			<br />
            		</div>
            	</div>
            </div>
            )
         }
         else if(this.state.en === 'true') {
            console.log(localStorage.getItem('ro'));
            console.log(localStorage.getItem('en'));
            console.log("Ce?" + this.state.ro)
            return(
               <div className = "NewsClass" style={bck}>

                    <h1 style={hs}>News</h1>
                    <br /><br /><br />
                    <p style={ps}>    In this section you will find the latest offers that our company has
                                                          offers them for sale, whether you want to buy a space or
                                                          just rent it. </p>
                    <br />
                    <p style={ps}>We always bring you the best offers, at the best prices on
                                                      market!</p>
                    <br />
                    <p style={ps}>We have a wide range of spaces located in various areas of the city, offering them
                                                      thus giving our customers the opportunity to opt for both peripheral areas, quiet,
                                                      as well as for central areas, accessible to anyone.</p>
                    <br />
                    <p style={ps}>The latest offers of the moment are the following and each can be found on the page corresponding to the category
                                                      which include: </p>
                    <br /><br />
                    <div style={rows}>
                        <div style={columns}>
                            <img style={imgs} src={require('./images/gars1.jpg')} width="400" height="350" />
                            <p style={ps}>Garsoniera de vanzare</p>
                            <br />
                        </div>
                        <div style={columns}>
                            <img style={imgs} src={require("./images/ap1.jpg")} width="400" height="350" />
                            <p style={ps}>Apartament de inchiriat</p>
                            <br />
                        </div>
                        <div style={columns}>
                            <img style={imgs} src={require("./images/casa1.jpg")} width="400" height="350" />
                            <p style={ps}>Casa de vanzare</p>
                            <br />
                        </div>
                    </div>
               </div>
             )
         }
    };
}

export default News