import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

class SellingHouse extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ro: localStorage.getItem('ro'),
            en: localStorage.getItem('en')
        };
        this.setStates = this.setStates.bind(this);
    }

    setStates(){
        this.setState({ro: localStorage.getItem('ro')})
        this.setState({en: localStorage.getItem('en')})
    }
    render() {
        const hs = {
            marginLeft: "30px",
            marginTop: "30px",
            textAlign: "justify"
        };
        const ps = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const imgs = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const columns = {
          float: 'left',
          width: '32%',
          padding: '5px'
        }

        const rows = {
          content: "",
          clear: 'both',
          display: 'table'
        }
        if(this.state.ro === 'true'){
        return(
        <div className = "SellingHouseClass">
            <h1 style={hs}>Vanzari case</h1>
            	<br /><br /><br />
            	<p style={ps}>    Aici veti putea gasi ofertele noastre de case pentru vanzare.
            	</p>
            	<br />
            	<p style={ps}>Nu toate  casele propuse spre vanzare sunt complet mobilate si utilate.
            	La cele care nu sunt mobilate se specifica acest lucru in descriere.</p>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/casa10.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Someseni, 90mp</p>
            			<p style={ps}>Pret: 600.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/casa9.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Zorilor, 110mp</p>
            			<p style={ps}>Pret: 710.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/casa8.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Manastur, 120mp</p>
            			<p style={ps}>Pret: 800.000 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/casa3.jpg")} width="400" height="300" />
            			<p style={ps}>Centru, 120mp</p>
            			<p style={ps}>Pret: 700.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/casa1.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Buna Ziua, 155mp</p>
            			<p style={ps}>Pret: 900.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/casa2.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Intre Lacuri, 149mp</p>
            			<p style={ps}>Pret: 910.000 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/casa5.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Someseni, 209mp</p>
            			<p style={ps}>Pret: 800.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/casa8.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Zorilor, 156mp</p>
            			<p style={ps}>Pret: 890.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/casa6.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Iris, 165mp</p>
            			<p style={ps}>Pret: 810.000 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            </div>
         )
         }
          else if(this.state.en === 'true') {
                 return(
                        <div className = "SellingHouseClass">
                            <h1 style={hs}>Selling Houses</h1>
                            	<br /><br /><br />
                            	<p style={ps}>    Here you can find our offers of houses for sale.
                            	</p>
                            	<br />
                            	<p style={ps}>Not all houses for sale are fully furnished and equipped.
                                                                          For those who are not furnished, this is specified in the description.</p>
                            	<br /><br />
                            	<div style={rows}>
                            	    <div style={columns}>
                            			<img style={imgs} src={require("./images/casa10.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Someseni, 90mp</p>
                            			<p style={ps}>Price: 600.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/casa9.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Zorilor, 110mp</p>
                            			<p style={ps}>Price: 710.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/casa8.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Manastur, 120mp</p>
                            			<p style={ps}>Price: 800.000 euro</p>
                            			<br />
                            		</div>
                            	</div>
                            	<br /><br />
                            	<div style={rows}>
                            	    <div style={columns}>
                            			<img style={imgs} src={require("./images/casa3.jpg")} width="400" height="300" />
                            			<p style={ps}>Centru, 120mp</p>
                            			<p style={ps}>Price: 700.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/casa1.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Buna Ziua, 155mp</p>
                            			<p style={ps}>Price: 900.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/casa2.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Intre Lacuri, 149mp</p>
                            			<p style={ps}>Price: 910.000 euro</p>
                            			<br />
                            		</div>
                            	</div>
                            	<br /><br />
                            	<div style={rows}>
                            	    <div style={columns}>
                            			<img style={imgs} src={require("./images/casa5.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Someseni, 209mp</p>
                            			<p style={ps}>Price: 800.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/casa8.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Zorilor, 156mp</p>
                            			<p style={ps}>Price: 890.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/casa6.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Iris, 165mp</p>
                            			<p style={ps}>Price: 810.000 euro</p>
                            			<br />
                            		</div>
                            	</div>
                            	<br /><br />
                            </div>
                         )
          }
    };
}

export default SellingHouse