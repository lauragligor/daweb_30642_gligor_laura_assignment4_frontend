import React, {useState, useEffect} from "react";
import {useNavigate} from "react-router-dom";
import Calendar from "react-calendar";
import {array} from "prop-types";
import moment from "moment";
import style from './css/style.css';
import {Line} from 'react-chartjs-2';
import Chart from 'chart.js/auto'
import {Bar} from "react-chartjs-2";



function Agent(){

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [file, setFile] = useState("");
    const [spacess, setSpaces] = useState("");
    const [value, setValue] = useState("");
    const [chartData, setChartData] = useState({})
    const navigate = useNavigate();
    let namee = "";
    let emaill = "";
    let password = "";
    let spaces = "";
    let role = "client";
    let id = "";
    const labelss = [];
    const datas =[];

    const nextDays = [
        '2022-05-01',
        '2022-05-05',
        '2022-05-08',
        '2022-05-07',
        '2022-05-18',
        '2022-05-17',
        '2022-05-28',
        '2022-05-29'
    ];

    const mark = [
        '07-05-2022'
    ];



    let newDaysObject = {};

    useEffect(() => {
        if (!localStorage.getItem('user-info')){
            navigate("/login")
        }

    }, [])

    let i = 0;

    const [value2, setValue2] = useState(0) ;

    function useForceUpdate(){

        if (value2>0) {
            if(value == "a1") {
                mark.push('07-05-2022');
                mark.push('15-05-2022');
                mark.push('21-05-2022');
                labelss.push('1');
                labelss.push('2');
                labelss.push('3');
                labelss.push('4');
                labelss.push('5');
                labelss.push('6');
                labelss.push('7');
                labelss.push('8');
                labelss.push('9');
                labelss.push('10');
                labelss.push('11');
                labelss.push('12');
                labelss.push('13');
                labelss.push('14');
                labelss.push('15');
                labelss.push('16');
                labelss.push('17');
                labelss.push('18');
                labelss.push('19');
                labelss.push('20');
                labelss.push('21');
                labelss.push('22');
                labelss.push('23');
                labelss.push('24');
                labelss.push('25');
                labelss.push('26');
                labelss.push('27');
                labelss.push('28');
                labelss.push('29');
                labelss.push('30');
                labelss.push('31');
                datas.push(5);
                datas.push(6);
                datas.push(9);
                datas.push(5);
                datas.push(8);
                datas.push(9);
                datas.push(3);
                datas.push(4);
                datas.push(6);
                datas.push(0);
                datas.push(9);
                datas.push(7);
                datas.push(10);
                datas.push(7);
                datas.push(4);
                datas.push(5);
                datas.push(4);
                datas.push(0);
                datas.push(1);
                datas.push(6);
                datas.push(0);
                datas.push(4);
                datas.push(7);
                datas.push(12);
                datas.push(2);
                datas.push(4);
                datas.push(1);
                datas.push(0);
                datas.push(6);
                datas.push(1);
                datas.push(2);
            }
            else{
                mark.push('04-05-2022');
                mark.push('19-05-2022');
                mark.push('29-05-2022');
                mark.push('09-05-2022');
                mark.push('27-05-2022');
                labelss.push('1');
                labelss.push('2');
                labelss.push('3');
                labelss.push('4');
                labelss.push('5');
                labelss.push('6');
                labelss.push('7');
                labelss.push('8');
                labelss.push('9');
                labelss.push('10');
                labelss.push('11');
                labelss.push('12');
                labelss.push('13');
                labelss.push('14');
                labelss.push('15');
                labelss.push('16');
                labelss.push('17');
                labelss.push('18');
                labelss.push('19');
                labelss.push('20');
                labelss.push('21');
                labelss.push('22');
                labelss.push('23');
                labelss.push('24');
                labelss.push('25');
                labelss.push('26');
                labelss.push('27');
                labelss.push('28');
                labelss.push('29');
                labelss.push('30');
                labelss.push('31');
                datas.push(3);
                datas.push(9);
                datas.push(4);
                datas.push(0);
                datas.push(6);
                datas.push(7);
                datas.push(9);
                datas.push(4);
                datas.push(3);
                datas.push(7);
                datas.push(8);
                datas.push(3);
                datas.push(1);
                datas.push(0);
                datas.push(6);
                datas.push(5);
                datas.push(8);
                datas.push(12);
                datas.push(9);
                datas.push(4);
                datas.push(1);
                datas.push(7);
                datas.push(0);
                datas.push(2);
                datas.push(7);
                datas.push(2);
                datas.push(9);
                datas.push(2);
                datas.push(0);
                datas.push(5);
                datas.push(4);
            }
        }

        return () => setValue2(value2 => value2 + 1);
    }

    const forceUpdate = useForceUpdate();

    function search(){
        let item = localStorage.getItem('user-info');
        console.log(item);
        let obj = JSON.parse(item);
        if (localStorage.getItem('user-info')) {
            id = obj.id;
            namee = obj.name;
            emaill = obj.email;
            spaces = obj.spaces;
        }
    }

    const handleChange = (e) => {
        setValue(e.target.value);
    };

    function selectDays() {
        mark.push('07-05-2022');
        mark.push('15-05-2022');
        mark.push('21-05-2022');
        console.log("aaaaa" + mark.toString())
    }

    const [valuey, onChange] = useState(new Date());

    return (

        <div className="col-sm-6 offset-sm-2">
            {search()}
            <h1>Agent Page</h1>
            <table width="140%">
                <br/>
                <br/>
                <tr>
                    <td>Visits:</td>
                    <td>Select a space to view</td>
                    <td><select id="dropdown" value={value} onChange={handleChange}>
                        <option value="g1">Garsoniera de vanzare, cart.Someseni, 38mp</option>
                        <option value="g2">Garsoniera de vanzare, cart. Buna Ziua, 35mp</option>
                        <option value="g3">Garsoniera de vanzare, cart. Zorilor, 33mp</option>
                        <option value="g4">Garsoniera de vanzare, Centru, 30mp</option>
                        <option value="g5">Garsoniera de vanzare, cart. Intre Lacuri, 37mp</option>
                        <option value="g6">Garsoniera de inchiriat, cart. Marasti, 34mp</option>
                        <option value="g7">Garsoniera de inchiriat, cart. Andrei Muresan, 34mp</option>
                        <option value="g8">Garsoniera de inchiriat, cart. Manastur, 34mp</option>
                        <option value="a1">Apartament de vanzare, cart. Zorilor, 55mp</option>
                        <option value="a2">Apartament de vanzare, cart. Faget, 50mp</option>
                        <option value="a3">Apartament de vanzare, cart. Iris, 65mp</option>
                        <option value="a4">Apartament de inchiriat, cart. Europa, 54mp</option>
                        <option value="a5">Apartament de inchiriat, cart. Manastur, 59mp</option>
                        <option value="a6">Apartament de inchiriat, cart. Someseni, 60mp</option>
                        <option value="a7">Apartament de inchiriat, cart. Buna Ziua, 48mp</option>
                        <option value="c1">Casa de vanzare, cart. Marasti, 78mp</option>
                        <option value="c2">Casa de vanzare, cart. Iris, 80mp</option>
                        <option value="c3">Casa de vanzare, cart. Zorilor, 68mp</option>
                        <option value="c4">Casa de vanzare, cart. Manastur, 76mp</option>
                        <option value="c5">Casa de inchiriat, cart. Buna Ziua, 80mp</option>
                        <option value="c6">Casa de inchiriat, cart. Marasti, 70mp</option>
                        <option value="c7">Casa de inchiriat, cart. Intre Lacuri, 88mp</option>
                        <option value="c8">Casa de inchiriat, cart. Europa, 75mp</option>
                        <option value="c9">Casa de inchiriat, cart. Buna ziua, 83mp</option>
                    </select></td>
                    <td>
                        <button className="btn btn-primary" onClick={forceUpdate} >Select space</button>
                    </td>
                </tr>
                <br/>
                <tr>
                    
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>
                        <Calendar
                            style={{ height: 500 }}
                            onChange={onChange}
                            value={valuey}
                            tileClassName={({ date, view }) => {
                                if(mark.find(x=>x===moment(date).format("DD-MM-YYYY"))){
                                    return  'highlight'
                                }
                            }}
                        >
                        </Calendar>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td>

                        <Bar
                            data={{
                                labels: labelss,
                                datasets: [{
                                    label: 'visits',
                                    data: datas,
                                    }],
                                }}
                        />
                    </td>

                    <td></td>
                </tr>
            </table>
        </div>
    )
}
export default Agent;