import React, {useState, useEffect} from "react";
import {useNavigate} from "react-router-dom";

function Client(){

    const [name, setName] = useState("");
    const [email, setEmail] = useState("");
    const [file, setFile] = useState("");
    const [spacess, setSpaces] = useState("");
    const navigate = useNavigate();
    let namee = "";
    let emaill = "";
    let password = "";
    let spaces = "";
    let role = "client";
    let id = "";

    useEffect(() => {
        if (!localStorage.getItem('user-info')){
            navigate("/login")
        }

    }, [])

    function search(){
        let item = localStorage.getItem('user-info');
        console.log(item);
        let obj = JSON.parse(item);
        if (localStorage.getItem('user-info')) {
            id = obj.id;
            namee = obj.name;
            emaill = obj.email;
            spaces = obj.spaces;
        }
    }

    function logOut(){
        localStorage.clear();
        navigate("/");
    }

    async function updateName(){
        let item = {id, name, email, password, spaces, role};
        let result = await fetch("http://localhost:8000/api/edit/" + id, {
            method: 'POST',
            body: JSON.stringify(item),
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json"
            }
        });
        result = await result.json();
        console.log(JSON.stringify(result))
        let obj = JSON.parse(JSON.stringify(result));
        localStorage.setItem("user-info", JSON.stringify(result));
        console.log(obj.name);
    }

    async function updateEmail(){
        let item = {id, name, email, password, spaces, role};
        let result = await fetch("http://localhost:8000/api/editm/" + id, {
            method: 'POST',
            body: JSON.stringify(item),
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json"
            }
        });
        result = await result.json();
        console.log("ceeee" + JSON.stringify(result))
        let obj = JSON.parse(JSON.stringify(result));
        localStorage.setItem("user-info", JSON.stringify(result));
        console.log(obj.email);
    }

    async function updateSpaces(){
        var options = document.getElementById('spaces').selectedOptions;
        var values = Array.from(options).map(({ value }) => value);
        spaces = values.toString();
        let item = {id, namee, email, password, spaces, role};
        let result = await fetch("http://localhost:8000/api/editt/" + id, {
            method: 'POST',
            body: JSON.stringify(item),
            headers:{
                "Content-Type":"application/json",
                "Accept":"application/json"
            }
        });
        result = await result.json();
        console.log(JSON.stringify(result));
        localStorage.setItem("user-info", JSON.stringify(result));
    }

    async function addFile(){
        const formData = new FormData();
        formData.append('name', name);
        formData.append('id', id);
        formData.append('email', email);
        formData.append('password', password);
        formData.append('role', role);
        formData.append('spaces', spaces);
        formData.append('file', file);
        console.log(file.toString());
        let result = await fetch("http://localhost:8000/api/file/" + id, {
            method: 'POST',
            body: formData
        });
        alert("File uploaded successfully");
    }

    return (
        <div className="col-sm-6 offset-sm-2">
            {search()}
            <h1>Client Page</h1>
            <table width="140%">
                <tr>
                    <td>Name:</td>
                    <td>{namee}</td>
                    <td><input type="text" className="form-control" value={name} onChange={(e) => setName(e.target.value)}/></td>
                    <td>
                        <button onClick={updateName} className="btn btn-primary">Update name</button>
                    </td>
                </tr>
                <br/>
                <tr>
                    <td>Email:</td>
                    <td>{emaill}</td>
                    <td><input type="text" className="form-control" value={email} onChange={(e) => setEmail(e.target.value)}/></td>
                    <td>
                        <button onClick={updateEmail} className="btn btn-primary">Update email</button>
                    </td>
                </tr>
                <br/>
                <tr>
                    <td>Spaces:</td>
                    <td>{spaces}</td>
                    <td><select name="spaces" id="spaces" multiple>
                        <option value="garsoniere de vanzare">Garsoniere de vanzare</option>
                        <option value="garsoniere de inchiriat">Garsoniere de inchiriat</option>
                        <option value="apartamente de vanzare">Apartamente de vanzare</option>
                        <option value="apartamente de inchiriat">Apartamente de inchiriat</option>
                        <option value="case de vanzare">Case de vanzare</option>
                        <option value="case de inchiriat">Case de inchiriat</option>
                    </select></td>
                    <td>
                        <button value={spacess} onClick={updateSpaces} className="btn btn-primary">Update spaces
                        </button>
                    </td>
                </tr>
                <br/>
                <tr>
                    <td>PDF file:</td>
                    <td>Give your GDPR consent here</td>
                    <td><input type="file" className="form-control" placeholder="file" onChange={(e) => setFile(e.target.files[0])}/></td>
                    <td><button className="btn btn-primary" onClick={addFile}>Add file</button></td>
                </tr>
                <br/>
                <tr>
                    <td>Visits:</td>
                    <td>Select a space to visit</td>
                    <td><select name="visit" id="visit">
                        <option value="g1">Garsoniera de vanzare, cart.Someseni, 38mp</option>
                        <option value="g2">Garsoniera de vanzare, cart. Buna Ziua, 35mp</option>
                        <option value="g3">Garsoniera de vanzare, cart. Zorilor, 33mp</option>
                        <option value="g4">Garsoniera de vanzare, Centru, 30mp</option>
                        <option value="g5">Garsoniera de vanzare, cart. Intre Lacuri, 37mp</option>
                        <option value="g6">Garsoniera de inchiriat, cart. Marasti, 34mp</option>
                        <option value="g7">Garsoniera de inchiriat, cart. Andrei Muresan, 34mp</option>
                        <option value="g8">Garsoniera de inchiriat, cart. Manastur, 34mp</option>
                        <option value="a1">Apartament de vanzare, cart. Zorilor, 55mp</option>
                        <option value="a2">Apartament de vanzare, cart. Faget, 50mp</option>
                        <option value="a3">Apartament de vanzare, cart. Iris, 65mp</option>
                        <option value="a4">Apartament de inchiriat, cart. Europa, 54mp</option>
                        <option value="a5">Apartament de inchiriat, cart. Manastur, 59mp</option>
                        <option value="a6">Apartament de inchiriat, cart. Someseni, 60mp</option>
                        <option value="a7">Apartament de inchiriat, cart. Buna Ziua, 48mp</option>
                        <option value="c1">Casa de vanzare, cart. Marasti, 78mp</option>
                        <option value="c2">Casa de vanzare, cart. Iris, 80mp</option>
                        <option value="c3">Casa de vanzare, cart. Zorilor, 68mp</option>
                        <option value="c4">Casa de vanzare, cart. Manastur, 76mp</option>
                        <option value="c5">Casa de inchiriat, cart. Buna Ziua, 80mp</option>
                        <option value="c6">Casa de inchiriat, cart. Marasti, 70mp</option>
                        <option value="c7">Casa de inchiriat, cart. Intre Lacuri, 88mp</option>
                        <option value="c8">Casa de inchiriat, cart. Europa, 75mp</option>
                        <option value="c9">Casa de inchiriat, cart. Buna ziua, 83mp</option>
                    </select></td>
                    <td>
                        <button className="btn btn-primary">Select space</button>
                    </td>
                </tr>
                <br/>
                <tr>
                    <td>Appointment:</td>
                    <td>Pick a date for your visit</td>
                    <td><input type="datetime-local"/></td>
                    <td><button className="btn btn-primary" >Select date</button></td>
                </tr>
                <br/>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td><button className="btn btn-primary" onClick={logOut}>Log Out</button></td>
                </tr>
            </table>
        </div>
    )
}
export default Client;