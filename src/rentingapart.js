import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

class RentingApart extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ro: localStorage.getItem('ro'),
            en: localStorage.getItem('en')
        };
        this.setStates = this.setStates.bind(this);
    }

    setStates(){
        this.setState({ro: localStorage.getItem('ro')})
        this.setState({en: localStorage.getItem('en')})
    }
    render() {
        const hs = {
            marginLeft: "30px",
            marginTop: "30px",
            textAlign: "justify"
        };
        const ps = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const imgs = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const columns = {
          float: 'left',
          width: '32%',
          padding: '5px'
        }

        const rows = {
          content: "",
          clear: 'both',
          display: 'table'
        }
        if(this.state.ro === 'true'){
        return(
        <div className = "RentingApartClass">
            <h1 style={hs}>Inchirieri apartamente</h1>
            	<br /><br /><br />
            	<p style={ps}>    Aici veti putea gasi ofertele noastre de apartamente pentru inchiriere.
            	</p>
            	<br />
            	<p style={ps}>Nu toate apartamentele propuse spre inchiriere sunt complet mobilate si utilate.
            	Cele care nu sunt mobilate se prespune ca se inchiriaza ca spatii comerciale.</p>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/ap1.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Someseni, 45mp</p>
            			<p style={ps}>Pret/luna: 250 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/ap2.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Zorilor, 50mp</p>
            			<p style={ps}>Pret/luna: 300 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/ap3.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Manastur, 39mp</p>
            			<p style={ps}>Pret/luna: 280 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/ap4.jpg")} width="400" height="300" />
            			<p style={ps}>Centru, 40mp</p>
            			<p style={ps}>Pret/luna: 330 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/ap5.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Buna Ziua, 55mp</p>
            			<p style={ps}>Pret/luna: 300 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/ap6.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Intre Lacuri, 49mp</p>
            			<p style={ps}>Pret/luna: 310 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/ap7.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Someseni, 39mp</p>
            			<p style={ps}>Pret/luna: 250 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/ap8.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Zorilor, 45mp</p>
            			<p style={ps}>Pret/luna: 320 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/ap9.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Iris, 55mp</p>
            			<p style={ps}>Pret/luna: 330 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            </div>
         )
         }
         else if(this.state.en === 'true') {
         return(
                 <div className = "RentingApartClass">
                     <h1 style={hs}>Renting apartments </h1>
                     	<br /><br /><br />
                     	<p style={ps}>    Here you can find our offers of apartments for rent.
                     	</p>
                     	<br />
                     	<p style={ps}>Not all apartments for rent are fully furnished and equipped.
                                                           Those that are not furnished are supposed to be rented as commercial spaces.</p>
                     	<br /><br />
                     	<div style={rows}>
                     	    <div style={columns}>
                     			<img style={imgs} src={require("./images/ap1.jpg")} width="400" height="300" />
                     			<p style={ps}>Cartier Someseni, 45mp</p>
                     			<p style={ps}>Price/month: 250 euro</p>
                     			<br />
                     		</div>
                     		<div style={columns}>
                     			<img style={imgs} src={require("./images/ap2.jpg")} width="400" height="300" />
                     			<p style={ps}>Cartier Zorilor, 50mp</p>
                     			<p style={ps}>Price/month: 300 euro</p>
                     			<br />
                     		</div>
                     		<div style={columns}>
                     			<img style={imgs} src={require("./images/ap3.jpg")} width="400" height="300" />
                     			<p style={ps}>Cartier Manastur, 39mp</p>
                     			<p style={ps}>Price/month: 280 euro</p>
                     			<br />
                     		</div>
                     	</div>
                     	<br /><br />
                     	<div style={rows}>
                     	    <div style={columns}>
                     			<img style={imgs} src={require("./images/ap4.jpg")} width="400" height="300" />
                     			<p style={ps}>Centru, 40mp</p>
                     			<p style={ps}>Price/month: 330 euro</p>
                     			<br />
                     		</div>
                     		<div style={columns}>
                     			<img style={imgs} src={require("./images/ap5.jpg")} width="400" height="300" />
                     			<p style={ps}>Cartier Buna Ziua, 55mp</p>
                     			<p style={ps}>Price/month: 300 euro</p>
                     			<br />
                     		</div>
                     		<div style={columns}>
                     			<img style={imgs} src={require("./images/ap6.jpg")} width="400" height="300" />
                     			<p style={ps}>Cartier Intre Lacuri, 49mp</p>
                     			<p style={ps}>Price/month: 310 euro</p>
                     			<br />
                     		</div>
                     	</div>
                     	<br /><br />
                     	<div style={rows}>
                     	    <div style={columns}>
                     			<img style={imgs} src={require("./images/ap7.jpg")} width="400" height="300" />
                     			<p style={ps}>Cartier Someseni, 39mp</p>
                     			<p style={ps}>Price/month: 250 euro</p>
                     			<br />
                     		</div>
                     		<div style={columns}>
                     			<img style={imgs} src={require("./images/ap8.jpg")} width="400" height="300" />
                     			<p style={ps}>Cartier Zorilor, 45mp</p>
                     			<p style={ps}>Price/month: 320 euro</p>
                     			<br />
                     		</div>
                     		<div style={columns}>
                     			<img style={imgs} src={require("./images/ap9.jpg")} width="400" height="300" />
                     			<p style={ps}>Cartier Iris, 55mp</p>
                     			<p style={ps}>Price/month: 330 euro</p>
                     			<br />
                     		</div>
                     	</div>
                     	<br /><br />
                     </div>
                  )
         }
    };
}

export default RentingApart