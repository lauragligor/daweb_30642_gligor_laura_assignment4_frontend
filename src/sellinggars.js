import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

class SellingGars extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ro: localStorage.getItem('ro'),
            en: localStorage.getItem('en')
        };
        this.setStates = this.setStates.bind(this);
    }

    setStates(){
        this.setState({ro: localStorage.getItem('ro')})
        this.setState({en: localStorage.getItem('en')})
    }
    render() {
        const hs = {
            marginLeft: "30px",
            marginTop: "30px",
            textAlign: "justify"
        };
        const ps = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const imgs = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const columns = {
          float: 'left',
          width: '32%',
          padding: '5px'
        }

        const rows = {
          content: "",
          clear: 'both',
          display: 'table'
        }
        if(this.state.ro === 'true'){
        return(
        <div className = "SellingGarsClass">
            <h1 style={hs}>Vanzari garsoniere</h1>
            	<br /><br /><br />
            	<p style={ps}>    Aici veti putea gasi ofertele noastre de garsoniere pentru vanzare.
            	</p>
            	<br />
            	<p style={ps}>Nu toate  garsonierele propuse spre vanzare sunt complet mobilate si utilate.
            	La cele care nu sunt mobilate se specifica acest lucru in descriere.</p>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/gars10.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Someseni, 15mp</p>
            			<p style={ps}>Pret: 100.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/gars9.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Zorilor, 23mp</p>
            			<p style={ps}>Pret: 120.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/gars8.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Manastur, 22mp</p>
            			<p style={ps}>Pret: 130.000 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/gars3.jpg")} width="400" height="300" />
            			<p style={ps}>Centru, 20mp</p>
            			<p style={ps}>Pret: 150.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/gars1.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Buna Ziua, 25mp</p>
            			<p style={ps}>Pret: 90.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/gars2.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Intre Lacuri, 21mp</p>
            			<p style={ps}>Pret: 80.000 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/gars5.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Someseni, 19mp</p>
            			<p style={ps}>Pret: 70.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/gars8.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Zorilor, 25mp</p>
            			<p style={ps}>Pret: 85.000 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/gars6.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Iris, 22mp</p>
            			<p style={ps}>Pret: 60.000 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            </div>
         )
         }
          else if(this.state.en === 'true') {
                return(
                        <div className = "SellingGarsClass">
                            <h1 style={hs}>Selling studios</h1>
                            	<br /><br /><br />
                            	<p style={ps}>    Here you can find our offers of studios for sale.
                            	</p>
                            	<br />
                            	<p style={ps}>Not all studios offered for sale are fully furnished and equipped.
                                                                          For those who are not furnished, this is specified in the description.</p>
                            	<br /><br />
                            	<div style={rows}>
                            	    <div style={columns}>
                            			<img style={imgs} src={require("./images/gars10.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Someseni, 15mp</p>
                            			<p style={ps}>Price: 100.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/gars9.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Zorilor, 23mp</p>
                            			<p style={ps}>Price: 120.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/gars8.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Manastur, 22mp</p>
                            			<p style={ps}>Price: 130.000 euro</p>
                            			<br />
                            		</div>
                            	</div>
                            	<br /><br />
                            	<div style={rows}>
                            	    <div style={columns}>
                            			<img style={imgs} src={require("./images/gars3.jpg")} width="400" height="300" />
                            			<p style={ps}>Centru, 20mp</p>
                            			<p style={ps}>Price: 150.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/gars1.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Buna Ziua, 25mp</p>
                            			<p style={ps}>Price: 90.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/gars2.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Intre Lacuri, 21mp</p>
                            			<p style={ps}>Price: 80.000 euro</p>
                            			<br />
                            		</div>
                            	</div>
                            	<br /><br />
                            	<div style={rows}>
                            	    <div style={columns}>
                            			<img style={imgs} src={require("./images/gars5.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Someseni, 19mp</p>
                            			<p style={ps}>Price: 70.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/gars8.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Zorilor, 25mp</p>
                            			<p style={ps}>Price: 85.000 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/gars6.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Iris, 22mp</p>
                            			<p style={ps}>Price: 60.000 euro</p>
                            			<br />
                            		</div>
                            	</div>
                            	<br /><br />
                            </div>
                         )
          }
    };
}

export default SellingGars