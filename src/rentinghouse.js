import React from 'react'
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'

class RentingHouse extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ro: localStorage.getItem('ro'),
            en: localStorage.getItem('en')
        };
        this.setStates = this.setStates.bind(this);
    }

    setStates(){
        this.setState({ro: localStorage.getItem('ro')})
        this.setState({en: localStorage.getItem('en')})
    }
    render() {
        const hs = {
            marginLeft: "30px",
            marginTop: "30px",
            textAlign: "justify"
        };
        const ps = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const imgs = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const columns = {
          float: 'left',
          width: '32%',
          padding: '5px'
        }

        const rows = {
          content: "",
          clear: 'both',
          display: 'table'
        }
        if(this.state.ro === 'true'){
        return(
        <div className = "RentingHouseClass">
            <h1 style={hs}>Inchirieri case</h1>
            	<br /><br /><br />
            	<p style={ps}>    Aici veti putea gasi ofertele noastre de case pentru inchiriere.
            	</p>
            	<br />
            	<p style={ps}>Nu toate casele propuse spre inchiriere sunt complet mobilate si utilate.
            	Cele care nu sunt mobilate se prespune ca se inchiriaza ca spatii comerciale.</p>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/casa1.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Someseni, 150mp</p>
            			<p style={ps}>Pret/luna: 870 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/casa2.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Zorilor, 135mp</p>
            			<p style={ps}>Pret/luna: 690 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/casa3.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Manastur, 127mp</p>
            			<p style={ps}>Pret/luna: 780 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/casa4.jpeg")} width="400" height="300" />
            			<p style={ps}>Centru, 120mp</p>
            			<p style={ps}>Pret/luna: 530 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/casa5.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Buna Ziua, 155mp</p>
            			<p style={ps}>Pret/luna: 700 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/casa6.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Intre Lacuri, 149mp</p>
            			<p style={ps}>Pret/luna: 810 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            	<div style={rows}>
            	    <div style={columns}>
            			<img style={imgs} src={require("./images/casa7.jpeg")} width="400" height="300" />
            			<p style={ps}>Cartier Someseni, 120mp</p>
            			<p style={ps}>Pret/luna: 730 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/casa8.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Zorilor, 130mp</p>
            			<p style={ps}>Pret/luna: 800 euro</p>
            			<br />
            		</div>
            		<div style={columns}>
            			<img style={imgs} src={require("./images/casa9.jpg")} width="400" height="300" />
            			<p style={ps}>Cartier Iris, 135mp</p>
            			<p style={ps}>Pret/luna: 700 euro</p>
            			<br />
            		</div>
            	</div>
            	<br /><br />
            </div>
         )
         }
          else if(this.state.en === 'true') {
                 return(
                        <div className = "RentingHouseClass">
                            <h1 style={hs}>Renting Houses</h1>
                            	<br /><br /><br />
                            	<p style={ps}>    Here you can find our offers of houses for rent.
                            	</p>
                            	<br />
                            	<p style={ps}>Not all houses for rent are fully furnished and equipped.
                                                                Those that are not furnished are supposed to be rented as commercial spaces.</p>
                            	<br /><br />
                            	<div style={rows}>
                            	    <div style={columns}>
                            			<img style={imgs} src={require("./images/casa1.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Someseni, 150mp</p>
                            			<p style={ps}>Price/month: 870 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/casa2.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Zorilor, 135mp</p>
                            			<p style={ps}>Price/month: 690 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/casa3.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Manastur, 127mp</p>
                            			<p style={ps}>Price/month: 780 euro</p>
                            			<br />
                            		</div>
                            	</div>
                            	<br /><br />
                            	<div style={rows}>
                            	    <div style={columns}>
                            			<img style={imgs} src={require("./images/casa4.jpeg")} width="400" height="300" />
                            			<p style={ps}>Centru, 120mp</p>
                            			<p style={ps}>Price/month: 530 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/casa5.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Buna Ziua, 155mp</p>
                            			<p style={ps}>Price/month: 700 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/casa6.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Intre Lacuri, 149mp</p>
                            			<p style={ps}>Price/month: 810 euro</p>
                            			<br />
                            		</div>
                            	</div>
                            	<br /><br />
                            	<div style={rows}>
                            	    <div style={columns}>
                            			<img style={imgs} src={require("./images/casa7.jpeg")} width="400" height="300" />
                            			<p style={ps}>Cartier Someseni, 120mp</p>
                            			<p style={ps}>Price/month: 730 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/casa8.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Zorilor, 130mp</p>
                            			<p style={ps}>Price/month: 800 euro</p>
                            			<br />
                            		</div>
                            		<div style={columns}>
                            			<img style={imgs} src={require("./images/casa9.jpg")} width="400" height="300" />
                            			<p style={ps}>Cartier Iris, 135mp</p>
                            			<p style={ps}>Price/month: 700 euro</p>
                            			<br />
                            		</div>
                            	</div>
                            	<br /><br />
                            </div>
                         )
          }
    };
}

export default RentingHouse