import React from 'react'

class Contact extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            ro: localStorage.getItem('ro'),
            en: localStorage.getItem('en')
        };
        this.setStates = this.setStates.bind(this);
    }

    setStates(){
        this.setState({ro: localStorage.getItem('ro')})
        this.setState({en: localStorage.getItem('en')})
    }

    showMessage(){
        alert("Your message has been sent!");
    }
    render() {
        const hs = {
            marginLeft: "30px",
            marginTop: "30px",
            textAlign: "justify"
        };
        const ps = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const imgs = {
            marginLeft: '30px',
            marginRight: '30px',
            textAlign: 'justify'
        }
        const columns = {
          float: 'left',
          width: '50%',
          padding: '5px'
        }

         const columnss = {
           float: 'right',
           width: '50%',
           padding: '50px'
         }

        const rows = {
          content: "",
          clear: 'both',
          display: 'table'
        }

        const inputtt = {
          width: '100%',
          padding: '12px',
          border: '1px solid #ccc',
          borderRadius: '4px',
          boxSizing: 'border-box',
          marginTop: '6px',
          marginBottom: '16px',
          resize: 'vertical'
        }
      const textareat = {
          width: '100%',
        padding: '12px',
        border: '1px solid #ccc',
        borderRadius: '4px',
        boxSizing: 'border-box',
        marginTop: '6px',
        marginBottom: '16px',
        resize: 'vertical'
      }

        const inputts = {
          backgroundColor: '#9F9A98',
          color: 'white',
          padding: '12px 20px',
          border: 'none',
          borderRadius: '4px',
          cursor: 'pointer'
        }

        const inputtsh = {
          backgroundColor: '#45a049'
        }

        const container = {
          borderRadius: '5px',
          backgroundColor: '#f2f2f2',
          padding: '20px'
        }


        if(this.state.ro === 'true'){
        return(
        <div style={rows} className = "ContactClass">
        <div style={columns}>
            <h1 style={hs}>Contact</h1>
            <br /><br />
            <h3 style={hs}>Telefon</h3>
            <p style={ps}> +40742564356
            </p>
            <br />
            <h3 style={hs}>Email</h3>
            <p style={ps}> myplace@gmail.com
            </p>
            <br />
            <h3 style={hs}>Adresa</h3>
            <p style={ps}> Strada Frunzisului, nr. 108, Cluj-Napoca, Romania
            </p>
            <br />
            </div>
            <div style={columnss}>
                <label for="name">Nume</label>
                    <input style={inputtt} type="text" id="name" name="name" placeholder="Numele dumneavoastra.." />
                <label for="mail">Email</label>
                    <input style={inputtt} type="text" id="mail" name="mail" placeholder="email@exemplu.com.." />
                <label for="subject">Subiect</label>
                     <input style={textareat} type="text" id="subject" name="subject" placeholder="Mesajul dumneavoastra.." />
                <input style={inputts}  type="submit" value="Trimite" onClick={() => this.showMessage()}/>
            </div>
            </div>

         )
         }
         else if(this.state.en === 'true') {
             return(
                    <div style={rows} className = "ContactClass">
                    <div style={columns}>
                        <h1 style={hs}>Contact</h1>
                        <br /><br />
                        <h3 style={hs}>Phone</h3>
                        <p style={ps}> +40742564356
                        </p>
                        <br />
                        <h3 style={hs}>Email</h3>
                        <p style={ps}> myplace@gmail.com
                        </p>
                        <br />
                        <h3 style={hs}>Address</h3>
                        <p style={ps}> Strada Frunzisului, nr. 108, Cluj-Napoca, Romania
                        </p>
                        <br />
                        </div>
                        <div style={columnss}>
                                        <label for="name">Name</label>
                                            <input style={inputtt} type="text" id="name" name="name" placeholder="Your name.." />
                                        <label for="mail">Email</label>
                                            <input style={inputtt} type="text" id="mail" name="mail" placeholder="email@example.com.." />
                                        <label for="subject">Subject</label>
                                             <input style={textareat} type="text" id="subject" name="subject" placeholder="Type your message here.." />
                                        <input style={inputts}  type="submit" value="Submit" onClick={() => this.showMessage()}/>
                                    </div>
                        </div>
                     )
         }
    };
}

export default Contact